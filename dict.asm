extern string_equals
global find_word
extern print_string
%define SHIFT 8

section .text

;rdi - pointer to a null-terminated string
;rsi - pointer to dictionary
find_word:
        xor rax, rax
	.loop:
                push rdi
                push rsi
                add rsi, SHIFT
                call string_equals
                pop rsi
                pop rdi
                cmp rax, 1
                je .return
                cmp qword[rsi], 0
                je .err
                mov rsi, [rsi]
                jmp .loop

	.return:
                mov rax, rsi
		ret

        .err:
                mov rax, 0
                ret
