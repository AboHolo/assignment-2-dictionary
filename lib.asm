%define SYS_WR 1
%define STDOUT 1
%define LINE_FEED 0xA
%define DECIMAL 10
%define SYS_RD 0
%define STDIN 0
%define SPACE 0x20
%define TAB 0x9
%define MINUS '-'
%define EXIT 60

section .text
 
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp	byte[rdi + rax], 0
        je	.end
        inc	rax
        jmp	.loop
	.end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rdi
    mov  rdx, rax
    mov  rsi, rdi
    mov  rax, SYS_WR
    mov  rdi, STDOUT
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, SYS_WR
    mov rdi, STDOUT
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov	rdi, LINE_FEED
	jmp	print_char
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov	rax, rdi
	mov	r10, DECIMAL
	dec rsp
	mov byte[rsp], 0
	mov rcx, 1
	.loop:
		xor rdx, rdx
		div	r10
		inc rcx
		add rdx, '0'
		dec rsp
		mov [rsp], dl
		cmp rax, 0
		jne .loop
	mov rdi, rsp
	push rcx
	call print_string
	pop rcx
	add rsp, rcx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi
	jnl .plus
	push rdi
	mov rdi, MINUS
	call print_char
	pop rdi
	neg rdi
	.plus:
		call print_uint
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	push rdi
	push rsi
	call string_length
	push rax
	mov rdi, rsi
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rax, rdx
	jne .neq
	xor rcx, rcx
	cmp rax, 0
	je .eq
	mov r9, rax
	.loop:
		mov al, [rdi+rcx]
		mov dl, [rsi+rcx]
		cmp rax, rdx
		jne .neq
		inc rcx
		cmp rcx, r9
		je .eq
		jmp .loop

	.eq:
		mov rax, 1
		ret
	.neq:
		mov rax, 0
		ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	mov rax, SYS_RD
	mov rdi, STDIN
	mov rdx, 1
	push 0
	mov rsi, rsp
	syscall
	pop rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	mov r9, 0
	mov rdx, 2
	.loop:
		push rdx
		push rsi
		push rdi
		call read_char
		mov r8, rax
		pop rdi
		pop rsi
		pop rdx
		cmp r8, 0 
		je .nullterm
		push rdi 
		mov rdi, r8
		call is_space
		pop rdi
		cmp rax, 0
		je .is_not_space
		cmp r9, 1 
		je .nullterm 
		jmp .loop
		.is_not_space:
			mov r9, 1
		cmp rdx, rsi
		ja .err
		mov [rdi + rdx - 2], r8 
		inc rdx
		jmp .loop
	.nullterm:
		mov rax, rdi
		mov byte[rdi + rdx - 2], 0 
		dec rdx
		dec rdx
		jmp .end
	.err:
		mov rax, 0
	.end:
    	ret
 
 is_space:
	cmp rdi, SPACE
	je .ws
	cmp rdi, TAB
	je .ws
	cmp rdi, LINE_FEED
	je .ws
	xor rax, rax
	ret
	.ws:
		mov al, 1
		ret


 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rdx, rdx
	mov r9, 10
	.loop:
		cmp byte [rdi], '0'
		jb .end
		cmp byte [rdi], '9'
		ja .end
		push rdx
		mul r9
		pop rdx
		add al, byte [rdi]
		sub al, '0'
		inc rdx
		inc rdi
		jmp .loop
	.end:
		ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	mov al, [rdi]
	cmp al, '-'
	jne .pos
	.neg:
		inc rdi
		call parse_uint
		cmp rdx, 0
		je .end
		inc rdx
		neg rax
		ret
	.pos:
		call parse_uint
	.end:
		ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rdx, rax
	jl .error
	xor rcx, rcx
	.loop:
		cmp rcx, rdx
		je .error
		mov rax, [rdi]
		mov [rsi], rax
		cmp byte[rdi], 0
		je .end
		inc rcx
		inc rsi
		inc rdi
		jmp .loop
	.error:
		mov rax, 0
		ret
	.end:
		mov rax, rcx
		ret
