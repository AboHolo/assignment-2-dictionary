%define buff 255

section .data
%include "words.inc"

section .bss
buffer: resb buff

section .rodata
out_of_bound: db "too long", 0
not_found: db "not found", 0

section .text
global _start
%include "lib.inc"
%include "dict.inc"

_start:
    mov rdi, buffer
    mov rsi, 255 
    push rdi
    call read_word
    pop rdi
    cmp rax, 0
    je .out_of_bound
    mov rsi, pointer
    call find_word
    cmp rax, 0
    je .not_found
    add rax, 8
    push rax
    mov rdi, rax
    call string_length  
    pop rcx
    lea rdi, [rcx + rax + 1] 
    call print_string
    xor rdi, rdi
    call exit
    .not_found:
        mov rdi, not_found
        call print_string
        call exit
    .out_of_bound:
        mov rdi, out_of_bound
        call print_string
        call exit


