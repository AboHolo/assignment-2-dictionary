ASM=nasm
ASMFLAGS=-f elf64
LD=ld

main.o: main.asm colon.inc words.inc
	$(ASM) $(ASMFLAGS) -o main.o main.asm

dict.o: dict.asm
	$(ASM) $(ASMFLAGS) -o dict.o dict.asm

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o lib.o lib.asm

clean:
	rm *.o program

program:  main.o lib.o dict.o
	$(LD) -o program main.o dict.o lib.o
